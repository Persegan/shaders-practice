#version 330 core
out vec4 FragColor;
in vec2 TexCoords;


uniform sampler2D iChannel0;


void main()
{             

    vec2 uv = TexCoords.xy;
    uv = floor(uv * 50.0f) / 50.0f;
    vec4 color = texture(iChannel0, uv*vec2(.2, 1.));

    FragColor += color;
}