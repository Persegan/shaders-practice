#version 330 core
out vec4 FragColor;
in vec2 TexCoords;


uniform sampler2D scene;



void main()
{             
    	vec2 uv = TexCoords.xy;
    	uv = floor(uv * 50.0f) / 50.0f;
    	vec3 color = texture(scene, uv).rgb;

    	FragColor += vec4(color, 1.0f);
}